var Jsonfile = require('jsonfile')
var intervalSwitch = false
var asyncInterval = require('asyncinterval')
var configFile = './conf.json'
var Twit = require('twit')
var track = []
var DM = []
var monitoredTwit = {}
var fs = require('fs')
var os = require('os')
var lineBreak = os.EOL
require('dotenv').load()

loadSettings(configFile)

var T = new Twit({
  consumer_key: process.env.CONSUMER_KEY,
  consumer_secret: process.env.CONSUMER_SECRET,
  access_token: process.env.ACCESS_TOKEN,
  access_token_secret: process.env.ACCESS_TOKEN_SECRET,
  timeout_ms: 60 * 1000 // optional HTTP request timeout to apply to all requests.
})

module.exports = function () {
  asyncInterval(function (done) {
    T.get('users/lookup', { screen_name: track.join() }, function (err, data, response) {
      if (!err) {
        var users = data
        for (var i in users) {
          if (users[i].status !== undefined) {
          var user = users[i]
          var statusId = user.status.id_str
          var statusTime = user.status.created_at
          var status = user.status.text
          var name = user.screen_name
          var message = '@' + name + '\n' + statusTime + '\n\n' + status

          if (monitoredTwit[name]) {
            if (monitoredTwit[name] !== statusId) {
              monitoredTwit[name] = statusId
              for (var x in DM) {
                T.post('direct_messages/new', {screen_name: DM[x], text: message}, function (err, data, response) {
                  if (err) {
                    console.log(err)
                  }
                })
              }
              done()
            }
          } else {
            monitoredTwit[name] = statusId
            for (var u in DM) {
              T.post('direct_messages/new', {screen_name: DM[u], text: message}, function (err, data, response) {
                if (err) {
                  console.log(err)
                }
              })
            }
            done()
          }
        }
        }
      } else {
        console.log(err)
        done()
      }
    })
  }, 1000, 2000)
}


module.exports.addTwitterSettings = function (req, res) {
  var file = 'CONSUMER_KEY=' + req.body.consumerKey + lineBreak + 'CONSUMER_SECRET=' + req.body.consumerSecret +
             lineBreak + 'ACCESS_TOKEN=' + req.body.accessToken + lineBreak + 'ACCESS_TOKEN_SECRET=' 
             + req.body.accessTokenSecret
  fs.writeFile('./.env', file, function (err) {
    if (!err) {
      res.status(200).end()
    } else {
      res.status(500).end()
    }
  })
}


module.exports.addTracking = function (req, res) {
  var config = Jsonfile.readFileSync(configFile)
  var trackUsers = req.body.track
  for (var i in trackUsers) {
    var user = trackUsers[i].toString().trim()
    if (config.track.indexOf(user) === -1) {
      config.track.push(user)
    }
  }
  Jsonfile.writeFile(configFile, config, function (err) {
    if (!err) {
      loadSettings(configFile)
      res.status(200).end()
    } else {
      res.status(500).end()
    }
  })
}

module.exports.listTracking = function (req, res) {
  var config = Jsonfile.readFileSync(configFile)
  res.send(JSON.stringify(config.track))
}

module.exports.removeTracking = function (req, res) {
  var config = Jsonfile.readFileSync(configFile)
  var screenName = req.body.screenName
  screenName = screenName.toString().trim()
  remove(config.track, screenName)
  Jsonfile.writeFile(configFile, config, function (err) {
    if (!err) {
      loadSettings(configFile)
      res.status(200).end()
    } else {
      res.status(500).end()
    }
  })
}

module.exports.addDM = function (req, res) {
  var config = Jsonfile.readFileSync(configFile)
  var usersToDM = req.body.DM
  for (var i in usersToDM) {
    var user = usersToDM[i].toString().trim()
    if (config.DM.indexOf(user) === -1) {
      config.DM.push(user)
    }
  }
  Jsonfile.writeFile(configFile, config, function (err) {
    if (!err) {
      loadSettings(configFile)
      res.status(200).end()
    } else {
      res.status(500).end()
    }
  })
}

module.exports.listDM = function (req, res) {
  var config = Jsonfile.readFileSync(configFile)
  res.send(JSON.stringify(config.DM))
}

module.exports.removeDM = function (req, res) {
  var config = Jsonfile.readFileSync(configFile)
  var screenName = req.body.screenName
  screenName = screenName.toString().trim()
  remove(config.DM, screenName)
  Jsonfile.writeFile(configFile, config, function (err) {
    if (!err) {
      loadSettings(configFile)
      res.status(200).end()
    } else {
      res.status(500).end()
    }
  })
}


module.exports.intervalSwitch = function (req, res) {
  intervalSwitch = req.body.iSwitch
  res.status(200).end()
}

function remove (arr, what) {
  var found = arr.indexOf(what)
  while (found !== -1) {
    arr.splice(found, 1)
    found = arr.indexOf(what)
  }
}

function loadSettings (configFile) {
  var config = Jsonfile.readFileSync(configFile)
  track = config.track
  DM = config.DM
}
