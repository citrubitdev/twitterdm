var express = require('express')
var bodyParser = require('body-parser')
var app = express()
require('dotenv').config()

app.use(bodyParser())// get html form data
app.use(bodyParser.json()) // get JSON data
app.use(express.static('public'))


app.get('/', function (req, res) {
  res.sendfile('./login.html', {root: __dirname})
})

app.post('/home', (req, res) => {
  var user = req.body.user
  var pass = req.body.pass
  if (user == process.env.user && pass == process.env.pass) {
    res.sendfile('./index.html', {root: __dirname})
  } else {
    res.sendfile('./login.html', {root: __dirname})
  }
})


require('./twitterMonitor')()
app.post('/updateTwitterSettings', require('./twitterMonitor').addTwitterSettings)
app.post('/addUsersToTrack', require('./twitterMonitor').addTracking)
app.post('/removeTracking', require('./twitterMonitor').removeTracking)
app.post('/addUsersToDM', require('./twitterMonitor').addDM)
app.post('/removeDM', require('./twitterMonitor').removeDM)
app.post('/intervalSwitch', require('./twitterMonitor').intervalSwitch)
app.get('/listTracking', require('./twitterMonitor').listTracking)
app.get('/listDM', require('./twitterMonitor').listDM)


app.listen(8081, '0.0.0.0', function () {
  console.log('Listening on 80')
})
